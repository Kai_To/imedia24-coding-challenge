# Stage 1: Build the application using a Gradle base image
FROM gradle:7.4.2-jdk8 AS build

# Set the working directory in the Gradle container
WORKDIR /app

# Copy the local Gradle configuration and source code to the container
COPY gradlew .
COPY gradle gradle
COPY build.gradle.kts settings.gradle.kts ./
COPY src src

# Grant execution permissions to the Gradle wrapper
RUN chmod +x ./gradlew

# Run the full Gradle build, which includes running tests
RUN ./gradlew build --no-daemon

# Build the bootable JAR file
# This step can be omitted if the bootJar task is already included in the build task
RUN ./gradlew bootJar --no-daemon

# Stage 2: Run the built application using an OpenJDK base image
FROM openjdk:8-jdk-slim

# Set the working directory in the OpenJDK container
WORKDIR /app

# Copy the built JAR file from the build stage into the OpenJDK container
COPY --from=build /app/build/libs/shop-0.0.1-SNAPSHOT.jar shop.jar

# Expose the port the application runs on
EXPOSE 8080

# Command to run the application
CMD ["java", "-jar", "shop.jar"]
