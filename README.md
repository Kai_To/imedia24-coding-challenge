# iMedia24 Coding challenge
# Overview
This is a Kotlin spring boot microservice for handling products.

## Prerequisites

Unless you are using Docker, ensure you have met the following requirements:
- JDK 8 installed on your machine 
- Gradle


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

To install the project, follow these steps:

```bash
git clone https://gitlab.com/Kai_To/imedia24-coding-challenge.git
cd imedia24-coding-challenge
./gradlew build
```
### Running the Application

To run the application, execute the following command:

```bash
./gradlew bootRun
```
The application should now be running on http://localhost:8080.

### Swagger API Documentation

This project is equipped with Swagger.

To access the Swagger UI and view the API documentation, Open your web browser and navigate to [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/).

### Deployment with Docker

To deploy this project using Docker, you can build a Docker image and then run it as a container. Follow these steps:

1. Build the Docker image:

```bash
docker build -t shop .
```

2. Once the image is built, run it as a container :

```bash
docker run -p 8080:8080 shop
```

This will start a Docker container running the application, which will be accessible at http://localhost:8080.


