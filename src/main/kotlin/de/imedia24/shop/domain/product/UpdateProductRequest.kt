package de.imedia24.shop.domain.product

import java.math.BigDecimal
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.NotBlank


data class UpdateProductRequest(

        @field:NotBlank(message = "Product name must not be empty")
        val name: String?,

        @field:NotBlank(message = "Product description must not be empty")
        val description: String?,

        @field:DecimalMin(value = "0.0", inclusive = false, message = "Product price must be greater than 0")
        val price: BigDecimal?
)