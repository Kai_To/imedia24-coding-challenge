package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class ProductRequest(

        @field:NotBlank(message = "Product sku must not be blank")
        val sku: String,
        @field:NotBlank(message = "Product name must not be blank")
        val name: String,
        @field:NotBlank(message = "Product description must not be blank")
        val description: String?,
        @field:DecimalMin(value = "0.0", inclusive = false, message = "Product price must be greater than 0")
        val price: BigDecimal,
        @field:NotNull
        val stock: Int
) {
    companion object {
        fun ProductRequest.toProductEntity() = ProductEntity(
                sku = sku,
                name = name,
                description = description,
                price = price,
                stock = stock,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )
    }
}

