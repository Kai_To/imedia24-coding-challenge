package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.UpdateProductRequest
import de.imedia24.shop.service.exception.ProductNotFoundException
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import java.util.stream.Collectors
import javax.persistence.EntityExistsException


@Service
class ProductService(private val productRepository: ProductRepository) {

    @Cacheable(value = ["product"], unless = "#result == null")
    fun findProductBySku(sku: String): ProductResponse {
        val productEntity = productRepository.findBySku(sku)
            ?: throw ProductNotFoundException(sku)
        return productEntity.toProductResponse()
    }

    @Cacheable(value = ["productList"], unless = "#result == null || #result.isEmpty()")
    fun findProductListBySkus(skus: Collection<String>): List<ProductResponse>? {
        val productEntities = productRepository.findBySkuIn(skus)
        return productEntities?.stream()?.map { entity -> entity.toProductResponse() }?.collect(Collectors.toList())
    }

    fun addProduct(productRequest: ProductRequest): ProductResponse {
        val existingProduct: ProductEntity? = productRepository.findBySku(productRequest.sku)
        if (existingProduct != null) {
            throw EntityExistsException("A product with SKU " + productRequest.sku + " already exists.")
        }
        val productEntity = productRequest.toProductEntity()
        return productRepository.save(productEntity).toProductResponse()
    }

    fun updateProduct(sku: String, updateProductRequest: UpdateProductRequest): ProductResponse {
        val originalProduct = productRepository.findBySku(sku)
            ?: throw ProductNotFoundException(sku)

        val patchedProduct = originalProduct.copy( // Entity class is immutable
            name = updateProductRequest.name ?: originalProduct.name,
            description = updateProductRequest.description ?: originalProduct.description,
            price = updateProductRequest.price ?: originalProduct.price
        )

        return productRepository.save(patchedProduct).toProductResponse()
    }
}
