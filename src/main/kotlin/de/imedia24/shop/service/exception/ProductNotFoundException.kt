package de.imedia24.shop.service.exception

class ProductNotFoundException(val sku: String) : RuntimeException("Product with SKU $sku not found")